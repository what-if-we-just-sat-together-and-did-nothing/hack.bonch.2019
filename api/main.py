from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api.endpoints import router as endpoints
from api.auth import router as auth

app = FastAPI()
app.include_router(endpoints, prefix='/api')
app.include_router(auth, prefix='/auth')

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
