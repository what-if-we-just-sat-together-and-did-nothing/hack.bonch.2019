docker build -t registry.gitlab.com/what-if-we-just-sat-together-and-did-nothing/hack.bonch.2019 .
docker push registry.gitlab.com/what-if-we-just-sat-together-and-did-nothing/hack.bonch.2019
scp docker-compose.yaml do:~/hb19
ssh do 'cd ~/hb19 && docker pull registry.gitlab.com/what-if-we-just-sat-together-and-did-nothing/hack.bonch.2019 && docker-compose up -d'
cd ../front
npm run build
scp -r build/* do:~/hb19/front
