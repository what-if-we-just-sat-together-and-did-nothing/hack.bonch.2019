from sqlalchemy import (
    Boolean, Column, ForeignKey, Integer, String,
    DateTime, Date, Boolean, text, UniqueConstraint
)
from sqlalchemy.orm import relationship

from .database import Base


class Event(Base):
    __tablename__ = 'events'

    statuses = ('waiting', 'agreement', 'agreed')

    id = Column(Integer, primary_key=True, index=True)
    status = Column(String, server_default=statuses[0])
    created = Column(DateTime, server_default=text("timezone('utc'::text, now())"), nullable=False)
    title = Column(String(32), nullable=False)
    description = Column(String, nullable=False)
    min_start_time = Column(Date)
    max_end_time = Column(Date)
    deleted = Column(Boolean, nullable=False, default=False)
    subscriptions = relationship("EventSubscription")
    owner_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    hoster_id = Column(Integer, ForeignKey('users.id'))
    subway = Column(String(25))
    address = Column(String(50))
    picture = Column(String)
    start_time = Column(DateTime)


class EventSubscription(Base):
    __tablename__ = 'events_subscriptions'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String(50), nullable=False)
    event_id = Column(Integer, ForeignKey('events.id'))

    __table_args__ = (
        UniqueConstraint('email', 'event_id', name='_email_event_uc'),
    )


class User(Base):
    __tablename__ = "users"

    types = ('creator', 'hoster')

    id = Column(Integer, primary_key=True, index=True)
    type = Column(String, server_default=types[0], nullable=False)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)
    first_name = Column(String(15), nullable=False)
    second_name = Column(String(15), nullable=False)
    photo = Column(String(50))
    bio = Column(String)
    vk_link = Column(String(25))
    twitter_link = Column(String(25))
    telegram_link = Column(String(25))
    events = relationship("Event", foreign_keys=[Event.owner_id], backref="owner")
    events_to_host = relationship("Event", foreign_keys=[Event.hoster_id], backref="hoster")
    token = Column(String(32))

