from typing import List
from datetime import datetime, timedelta

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import func
from sqlalchemy.orm import Session
from starlette.status import HTTP_400_BAD_REQUEST

from . import schemas
from .database import (
    get_db, model_create, model_list, model_delete, model_count, model_get
)
from . import models
from .auth import oauth2_scheme, get_current_user

router = APIRouter()


@router.post('/events', response_model=schemas.Event)
def create_event(event: schemas.EventCreate, db: Session = Depends(get_db), user: models.User = Depends(get_current_user)):
    return model_create(db, models.Event, event)


@router.get('/events')
def get_events(db: Session = Depends(get_db)):
    events = model_list(db, models.Event)
    result = []
    for event in events:
        # TODO: сделать через pydantic, а не этими костылями
        event_dict = schemas.Event.from_orm(event).dict()
        event_dict['visitors'] = len(event.subscriptions)
        result.append(event_dict)
    return result


@router.delete('/events/{event_id}', response_model=schemas.Event)
def cancel_event(event_id: int, db: Session = Depends(get_db), user: models.User = Depends(get_current_user)):
    return model_delete(db=db, model=models.Event, model_id=event_id, soft_delete=True)


@router.post('/events/{event_id}/subscribtions', response_model=schemas.EventSubscribe)
def event_subscribe(event_id: int, request: schemas.EventSubscribe, db: Session = Depends(get_db)):
    return model_create(db, models.EventSubscription, request, event_id=event_id)


@router.get('/events/{event_id}/subscribtions/count', response_model=schemas.SubscribtionsCount)
def event_subscribtions_count(event_id: int, db: Session = Depends(get_db)):
    return {'count': model_count(db, models.EventSubscription)}


@router.get('/events/popular')
def get_popular_events(db: Session = Depends(get_db)):
    # TODO: тут должен быть страшный, но эффективный sql запрос
    sq = db.query(
        models.EventSubscription.event_id,
        func.count(models.EventSubscription.event_id).label('count')
    ).group_by(models.EventSubscription.event_id).subquery()
    result_query = db.query(models.Event, sq.c.count).join(sq, sq.c.event_id == models.Event.id)

    result_rated = []
    for event, subscribtions_count in result_query:
        days = (datetime.utcnow() - event.created).total_seconds() / timedelta(days=1).total_seconds()
        result_rated.append((event, subscribtions_count, subscribtions_count/days if days > 0 else 0))

    result_sorted = sorted(result_rated, key=lambda x: x[2], reverse=True)

    result = []
    for event, subscribtions_count, _ in result_sorted[:5]:
        event_dict = schemas.Event.from_orm(event).dict()
        event_dict['visitors'] = subscribtions_count
        result.append(event_dict)
    return result


@router.get('/users/{user_id}', response_model=schemas.User)
def get_user(user_id: int, db: Session = Depends(get_db), user: models.User = Depends(get_current_user)):
    user_dict = schemas.User.from_orm(model_get(db, models.User, id=user_id)).dict()
    user_dict['events'] = []
    if user.id == user_id:
        if user.type == models.User.types[0]:
            user_dict['events'] = [schemas.Event.from_orm(i) for i in model_list(db, models.Event, owner_id=user_id)]
        else:
            user_dict['events'] = [schemas.Event.from_orm(i) for i in model_list(db, models.Event, hoster_id=user_id)]
    return user_dict


@router.post('/events/{event_id}/host', response_model=schemas.Event)
def host_event(event_id: int, db: Session = Depends(get_db), user: models.User = Depends(get_current_user)):
    event = model_get(db, models.Event, id=event_id)
    if event.status != models.Event.statuses[0]:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="event_already_hosting",
        )
    event.hoster_id = user.id
    event.status = models.Event.statuses[1]
    db.commit()
    return event
