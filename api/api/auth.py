import hashlib
import binascii
import string
import secrets
import os

from fastapi import Depends, APIRouter, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from starlette.status import HTTP_401_UNAUTHORIZED

from . import models
from . import schemas
from .database import get_db, model_create, model_count, model_get

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")


def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    user = model_get(db, models.User, token=token)
    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return user


def parse_password(password):
    return password.split('urf-8')


def format_password(password):
    salt = binascii.hexlify(os.urandom(32)).decode('utf-8')
    iterations = 100000
    password_hash = hash_password(password, salt, iterations)
    return f'{salt}${iterations}${password_hash}'


def hash_password(password, salt, iterations):
    return binascii.hexlify(hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        salt.encode('utf-8'),
        iterations
    )).decode('utf-8')


def gen_token(length):
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for _ in range(length))


@router.post('/token', response_model=schemas.Login)
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = model_get(db, models.User, email=form_data.username)
    if not user:
        raise HTTPException(status_code=400, detail='Incorrect username or password')
    salt, iterations, hash_ = user.password.split('$')
    hashed_password = hash_password(form_data.password, salt, int(iterations))
    if hashed_password != hash_:
        raise HTTPException(status_code=400, detail='Incorrect username or password')

    user.token = gen_token(32)
    db.commit()
    return {'access_token': user.token, 'token_type': 'bearer', 'user_id': user.id}


@router.post("/register", response_model=schemas.User)
async def register(user: schemas.UserCreate, db: Session = Depends(get_db)):
    user.password = format_password(user.password)
    return model_create(db, models.User, user)
