from typing import List
from datetime import date, datetime

from pydantic import BaseModel, EmailStr


class EventBase(BaseModel):
    title: str
    description: str
    owner_id: int
    min_start_time: date = None
    max_end_time: date = None
    subway: str = None
    address: str = None


class EventCreate(EventBase):
    pass


class Event(EventBase):
    id: int
    created: datetime
    deleted: bool
    status: str
    picture: str = None
    start_time: datetime = None
    hoster_id: int = None

    class Config:
        orm_mode = True


class EventSubscribe(BaseModel):
    email: EmailStr

    class Config:
        orm_mode = True

class UserBase(BaseModel):
    type: str = None
    email: EmailStr
    first_name: str
    second_name: str
    photo: str = None
    bio: str = None
    vk_link: str = None
    twitter_link: str = None
    telegram_link: str = None


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    events: List[Event]

    class Config:
        orm_mode = True


class Login(BaseModel):
    access_token: str
    token_type: str
    user_id: int


class SubscribtionsCount(BaseModel):
    count: int
