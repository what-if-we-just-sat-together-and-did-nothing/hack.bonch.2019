import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(
    os.getenv('DB_URL') #, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def model_create(db, model, item, **additional_fields):
    record = model(**{**item.dict(), **additional_fields})
    db.add(record)
    db.commit()
    db.refresh(record)
    return record


def model_get(db, model, **params):
    return db.query(model).filter_by(**params).first()


def model_list(db, model, **params):
    q = db.query(model)
    if params:
        q = q.filter_by(**params)
    return q.all()

def model_count(db, model):
    return db.query(model).count()


def model_delete(db, model, model_id, soft_delete=False) -> int:
    record = db.query(model).filter_by(id=model_id).first()
    if soft_delete:
        record.deleted = True
    else:
        record.delete()
    db.add(record)
    db.commit()
    return record

