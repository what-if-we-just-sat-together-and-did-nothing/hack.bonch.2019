ARG POETRY_VERSION=0.12.17

FROM python:3.7.5-alpine3.10

# install and configure poetry dependency manager
RUN apk add --no-cache curl && \
    curl -L -o get-poetry.py \
         https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py && \
    python get-poetry.py --version=${POETRY_VERSION} -y  && \
    $HOME/.poetry/bin/poetry config settings.virtualenvs.create false

# install system packages
RUN apk update && apk add --no-cache postgresql-dev build-base

WORKDIR /opt/project

# install dependencies
COPY pyproject.toml /opt/project/
COPY poetry.lock /opt/project/
RUN $HOME/.poetry/bin/poetry install --no-dev --no-interaction

CMD uvicorn main:app --host 0.0.0.0
