import axios from "axios";
import store from "../store";
import { getUser } from "../store/actions";

export const register = async (onSuccess, user) => {
    const userData = {
        ...user,
        first_name: user.firstName,
        second_name: user.secondName
    };
    delete userData.firstName;
    delete userData.secondName;
    const response = await axios.post(
        "https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/auth/register",
        userData
    );
    store.dispatch(
        getUser({
            user: {
                ...user,
                accessToken: response.data.access_token,
                id: response.data.user_id
            }
        })
    );
    onSuccess();
};

export const login = async (onSuccess, user) => {
    const form = new FormData();
    form.append("username", user.email);
    form.append("password", user.password);
    const response = await axios.post(
        "https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/auth/token",
        form
    );
    store.dispatch(
        getUser({
            user: {
                accessToken: response.data.access_token,
                id: response.data.user_id
            }
        })
    );
    localStorage.setItem("accessToken", response.data.access_token);
    localStorage.setItem("userId", response.data.user_id);
    onSuccess();
};

export const getProfile = async user => {
    console.log(user);
    const response = await axios.get(
        `https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/users/${user.id}`
    );
    const { data } = response;
    [
        "first_name",
        "is_active",
        "second_name",
        "telegram_link",
        "twitter_link",
        "vk_link"
    ].forEach(field => {
        delete data[field];
    });
    store.dispatch(
        getUser({
            user: {
                ...data,
                firstName: data.first_name,
                isActive: data.is_active,
                secondName: data.second_name,
                telegram: data.telegram_link,
                twitter: data.twitter_link,
                vk: data.vk_link,
            }
        })
    );
};
