import axios from "axios";
import store from "../store";
import { getEvents, addEvent, getPopularEvents } from "../store/actions";

export const fetchEvents = async () => {
    const response = await axios.get(
        "https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/events"
    );
    store.dispatch(getEvents({ events: response.data }));
};

export const createEvent = async (event) => {
    const response = await axios.post(
        "https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/events", event
    );
    store.dispatch(addEvent({ event: response.data }));
};

export const fetchPopularEvents = async (event) => {
    const response = await axios.get(
        "https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/events/popular"
    );
    store.dispatch(getPopularEvents({ events: response.data }));
};
