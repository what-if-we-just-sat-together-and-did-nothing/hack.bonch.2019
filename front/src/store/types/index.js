export const GET_EVENTS = 'GET_EVENTS';
export const ADD_EVENT = 'ADD_EVENT';
export const GET_POPULAR_EVENTS = 'GET_POPULAR_EVENTS';
export const GET_USER = 'GET_USER';
