import { GET_USER } from "../types";

const getInitialState = () => {
    const userId = localStorage.getItem('userId');
    const accessToken = localStorage.getItem('accessToken');
    return {
        bio: '',
        vk: '',
        twitter: '',
        telegram: '',
        avatar: '',
        email: '',
        phone: '',
        id:  userId !== 'undefined' ? userId : '',
        accessToken: accessToken !== 'undefined' ? accessToken : ''  
    }
}

export const user = (state = getInitialState(),  action) => {
    switch (action.type) {
        case GET_USER: {
            return {
                ...state,
                ...action.user
            };
        }
        default: {
            return {
                ...state
            }
        }
    }
};
