import { events } from "./event";
import { user } from "./user";
import { combineReducers } from "redux";

export default combineReducers({user, events});
