import { GET_EVENTS, ADD_EVENT, GET_POPULAR_EVENTS } from "../types";

const getInitialState = () => {
    return {
        events: [],
        popularEvents: []
    }
}

export const events = (state = getInitialState(), action) => {
    switch (action.type) {
        case GET_EVENTS: {
            return {
                ...state,
                events: action.events
            };
        }
        case GET_POPULAR_EVENTS: {
            return {
                ...state,
                popularEvents: action.events
            }
        }
        case ADD_EVENT: {
            return [
                ...state, action.event
            ];
        }
        default: {
            return {...state};
        }
    }
};
