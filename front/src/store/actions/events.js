import { GET_EVENTS, ADD_EVENT, GET_POPULAR_EVENTS } from "../types";
export const getEvents = data => {
    return {
        type: GET_EVENTS,
        ...data
    };
};

export const addEvent = data => {
    return {
        type: ADD_EVENT,
        ...data
    };
};

export const getPopularEvents = data => {
    return {
        type: GET_POPULAR_EVENTS,
        ...data
    };
};
