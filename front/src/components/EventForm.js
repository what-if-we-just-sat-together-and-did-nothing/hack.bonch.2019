import React, { useState } from "react";
import propTypes from "prop-types";
import { InputGroup, FormGroup, Label, Button } from "@blueprintjs/core";
import { Classes } from "@blueprintjs/core";
import { DateInput } from "@blueprintjs/datetime";
import moment from "moment";

const DATE_FORMAT = "YYYY-MM-DD";

const defaultEventObject = {
    title: "",
    description: "",
    min_start_time: "",
    max_end_time: ""
};

const EventForm = ({ onCreate, isButtonLoading }) => {
    const [eventInfo, setEventInfo] = useState({ ...defaultEventObject });
    const min_start_time = eventInfo.min_start_time
        ? moment(eventInfo.min_start_time, DATE_FORMAT).toDate()
        : null;
    const max_end_time = eventInfo.max_end_time
        ? moment(eventInfo.max_end_time, DATE_FORMAT).toDate()
        : null;
    return (
        <FormGroup className="form-group">
            <Label>
                Title
                <InputGroup
                    value={eventInfo.title}
                    onChange={e =>
                        setEventInfo({ ...eventInfo, title: e.target.value })
                    }
                    placeholder="Title"
                />
            </Label>
            <Label>
                Description
                <InputGroup
                    value={eventInfo.description}
                    onChange={e =>
                        setEventInfo({
                            ...eventInfo,
                            description: e.target.value
                        })
                    }
                    placeholder="Description"
                />
            </Label>
            <div className="event-form-date-picker">
                <Label>
                    Start date
                    <DateInput
                        className={Classes.ELEVATION_1}
                        value={min_start_time}
                        parseDate={str => moment(str)}
                        formatDate={date => moment(date).format(DATE_FORMAT)}
                        onChange={date =>
                            setEventInfo({
                                ...eventInfo,
                                min_start_time: moment(date).format(DATE_FORMAT)
                            })
                        }
                        placeholder="Start date"
                    />
                </Label>
                <Label>
                    Finish date
                    <DateInput
                        className={Classes.ELEVATION_1}
                        value={max_end_time}
                        parseDate={str => moment(str)}
                        formatDate={date => moment(date).format(DATE_FORMAT)}
                        onChange={date =>
                            setEventInfo({
                                ...eventInfo,
                                max_end_time: moment(date).format(DATE_FORMAT)
                            })
                        }
                        placeholder="End date"
                    />
                </Label>
            </div>
            <Button
                loading={isButtonLoading}
                onClick={onCreate.bind(null, eventInfo)}
            >
                Add event
            </Button>
        </FormGroup>
    );
};

EventForm.propTypes = {
    onCreate: propTypes.func.isRequired
};

EventForm.defaultsTypes = {
    isButtonLoading: false
};

export default EventForm;
