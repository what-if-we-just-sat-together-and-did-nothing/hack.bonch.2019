import React from 'react';
import  moment from 'moment';

import {fetchEvents} from '../api/events';
import { Scrollbars } from 'react-custom-scrollbars';
import Card from '../components/Card';

import '../styles/EventTable.scss';

export default class EventTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showedEvents: []
        };
    }

    componentDidMount() {
        fetchEvents();
    }

    render() {
        const {events} = this.props;
        const dateFormatter = (event) => moment(event.start_time).format('DD-MM-YYYY, HH:MM');
        return (
            <div className="event-table">
                <Scrollbars autoHeight autoHeightMax="36rem">
                    {events.map((event) =>
                        <Card key={event.id} name={event.title} address={event.address} subway={event.subway} desc={event.description}
                              date={dateFormatter(event)} img={event.picture} visitors={event.visitors} status={event.status}/>
                    )}
                </Scrollbars>

            </div>
        );
    }

}
