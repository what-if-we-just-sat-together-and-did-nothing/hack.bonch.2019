import React, { useState } from "react";
import store from "../store";
import { Provider } from "react-redux";

import { Icon, Button, Toaster, Toast, Intent } from "@blueprintjs/core";
import { login, register } from "../api/user";

import IndexPage from "../views/IndexPage";
import ProfilePage from "../views/ProfilePage";
import MapPage from "../views/MapPage";
import { connect } from "react-redux";
import LoginPage from "./Login/LoginPage";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom";
import "../styles/App.scss";
import NavIcon from "../assets/event.svg";

function RouterHandler({ user }) {
    const [isLoginPageOpen, setIsLoginPageOpen] = useState(false);
    const [wasSuccessfullySignIn, setWasSuccessfullySignIn] = useState(false);
    const onSignUp = user => {
        register(() => {
            setIsLoginPageOpen(false);
            setWasSuccessfullySignIn(true);
        }, user);
    };
    return (
        <>
            <Router>
                <div className="app-navbar">
                    <nav>
                        <div className="logo">
                            <Link to="/">
                                <img src={NavIcon} />
                            </Link>
                            <p>Event Feed</p>
                        </div>
                        <div className="map-link">
                            <Link to="/map">
                                <button className="bp3-button">
                                    <Icon icon="map" color="#FCBE19" />
                                    Карта
                                </button>
                            </Link>
                        </div>
                        {user.accessToken && (
                            <div className="profile">
                                <Link to="/profile">
                                    <button className="bp3-button">
                                        <Icon icon="user" color="#FCBE19" />
                                        Профиль
                                    </button>
                                </Link>
                            </div>
                        )}
                        {!user.accessToken && (
                            <div className="profile">
                                <Button
                                    onClick={setIsLoginPageOpen.bind(
                                        null,
                                        true
                                    )}
                                >
                                    <Icon icon="user" color="#FCBE19" />
                                    Логин
                                </Button>
                                <LoginPage
                                    isOpen={isLoginPageOpen}
                                    onSignIn={login.bind(
                                        null,
                                        setIsLoginPageOpen.bind(null, false)
                                    )}
                                    onSignUp={onSignUp}
                                    onClose={setIsLoginPageOpen.bind(
                                        null,
                                        false
                                    )}
                                ></LoginPage>
                            </div>
                        )}
                    </nav>
                </div>
                <div className="app-content">
                    <Switch>
                        <Route exact path="/">
                            <IndexPage />
                        </Route>
                        {(!user.id || !user.accessToken) && (
                            <Redirect from="/profile" to="/" />
                        )}
                        <Route path="/profile">
                            <ProfilePage />
                        </Route>
                        <Route path="/map">
                            <MapPage />
                        </Route>
                    </Switch>
                </div>
            </Router>
            <Toaster>
                {wasSuccessfullySignIn && (
                    <Toast
                        intent={Intent.SUCCESS}
                        message="Successfully sign up. You can sign in now."
                        onDismiss={() => setWasSuccessfullySignIn(false)}
                    ></Toast>
                )}
            </Toaster>
        </>
    );
}

function App() {
    const Routes = connect(mapStateToProps, mapDispatchToProps)(RouterHandler);
    return (
        <Provider store={store}>
            <div className="App">
                <Routes />
            </div>
        </Provider>
    );
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default App;
