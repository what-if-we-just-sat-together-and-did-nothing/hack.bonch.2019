import React, { useState } from "react";
import propTypes from "prop-types";
import { InputGroup, FormGroup, Label, Button } from "@blueprintjs/core";

const defaultUserObject = {
    email: "",
    password: ""
};

const SignIn = ({ onSignIn, isButtonLoading }) => {
    const [userInfo, setUserInfo] = useState({ ...defaultUserObject });
    return (
        <FormGroup className="form-group">
            <Label>
                Email (required)
                <InputGroup
                    value={userInfo.email}
                    onChange={e =>
                        setUserInfo({ ...userInfo, email: e.target.value })
                    }
                    placeholder="Email"
                />
            </Label>
            <Label>
                Password (required)
                <InputGroup
                    value={userInfo.password}
                    onChange={e =>
                        setUserInfo({ ...userInfo, password: e.target.value })
                    }
                    type="password"
                    placeholder="Password"
                />
            </Label>
            <Button loading={isButtonLoading} onClick={() => onSignIn(userInfo)}>
                Sign in
            </Button>
        </FormGroup>
    );
};

SignIn.propTypes = {
    onSignIn: propTypes.func.isRequired,
    isButtonLoading: propTypes.bool
};

SignIn.defaultTypes = {
    isButtonLoading: false
};

export default SignIn;
