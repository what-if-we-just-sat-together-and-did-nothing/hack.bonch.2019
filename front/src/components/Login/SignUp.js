import React, { useState } from "react";
import propTypes from "prop-types";
import {
    InputGroup,
    FormGroup,
    RadioGroup,
    Radio,
    Label,
    Button
} from "@blueprintjs/core";

const EVENT_CREATOR = "Creator";
const EVENT_HOSTER = "Hoster";

const defaultUserObject = {
    email: "",
    password: "",
    firstName: "",
    secondName: "",
    patronymic: "",
    type: EVENT_CREATOR
};

const SignUp = ({ onSignUp, isButtonLoading }) => {
    const [userInfo, setUserInfo] = useState({ ...defaultUserObject });
    return (
        <FormGroup className="form-group">
            <Label>
                Email (required)
                <InputGroup
                    value={userInfo.email}
                    onChange={e =>
                        setUserInfo({ ...userInfo, email: e.target.value })
                    }
                    placeholder="Email"
                />
            </Label>
            <Label>
                Password (required)
                <InputGroup
                    value={userInfo.password}
                    onChange={e =>
                        setUserInfo({ ...userInfo, password: e.target.value })
                    }
                    type="password"
                    placeholder="Password"
                />
            </Label>
            <Label>
                First name (required)
                <InputGroup
                    value={userInfo.firstName}
                    onChange={e =>
                        setUserInfo({ ...userInfo, firstName: e.target.value })
                    }
                    placeholder="First name"
                />
            </Label>
            <Label>
                Second name (required)
                <InputGroup
                    value={userInfo.secondName}
                    onChange={e =>
                        setUserInfo({ ...userInfo, secondName: e.target.value })
                    }
                    placeholder="Second name"
                />
            </Label>
            <Label>
                Patronymic
                <InputGroup
                    value={userInfo.patronymic}
                    onChange={e =>
                        setUserInfo({ ...userInfo, patronymic: e.target.value })
                    }
                    placeholder="Patronymic"
                />
            </Label>
            <Label>
            Account type
            <RadioGroup
                className='account-type-radio-buttons'
                onChange={value =>
                    setUserInfo({
                        ...userInfo,
                        type: value.currentTarget.value
                    })
                }
                selectedValue={userInfo.type}
                inline
            >
                <Radio value={EVENT_CREATOR} label={EVENT_CREATOR} />
                <Radio value={EVENT_HOSTER} label={EVENT_HOSTER} />
            </RadioGroup>
            </Label>
            <Button loading={isButtonLoading} onClick={() => onSignUp(userInfo)}>
                Sign up
            </Button>
        </FormGroup>
    );
};

SignUp.propTypes = {
    onSignUp: propTypes.func.isRequired,
    isButtonLoading: propTypes.bool
};

SignUp.defaultsTypes = {
    isButtonLoading: false
};

export default SignUp;
