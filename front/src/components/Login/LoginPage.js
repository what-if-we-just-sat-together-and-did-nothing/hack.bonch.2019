import React, { useState } from "react";
import propTypes from "prop-types";
import { Dialog, Tab, Tabs, Toast, Intent, Toaster } from "@blueprintjs/core";
import SignUp from "./SignUp";
import SignIn from "./SignIn";
import "../../styles/LoginPage.scss";

const SIGN_IN = "Sign In";
const SIGN_UP = "Sign Up";

const userValidator = validateType => user => {
    if (validateType === SIGN_IN) {
        return user.email && user.password;
    }
    return user.email && user.password && user.firstName && user.secondName;
};

const onAttemptSubmit = (onWrongValidate, onSubmit, validator) => user => {
    if (!validator(user)) {
        onWrongValidate();
        return;
    }
    onSubmit(user);
};

const signInValidator = userValidator(SIGN_IN);
const signUpValidator = userValidator(SIGN_UP);

const LoginPage = ({ onClose, isOpen, onSignIn, onSignUp, isInProgress }) => {
    const [tabId, setTabId] = useState(SIGN_IN);
    const [showError, setShowError] = useState(false);
    const onAttemptSignIn = onAttemptSubmit(
        setShowError.bind(null, true),
        onSignIn,
        signInValidator
    );
    const onAttemptSignUp = onAttemptSubmit(
        setShowError.bind(null, true),
        onSignUp,
        signUpValidator
    );
    return (
        <>
            <Dialog
                className="login-dialog"
                isOpen={isOpen}
                onClose={onClose}
                title={tabId}
            >
                <Tabs
                    renderActiveTabPanelOnly
                    className="login-tabs"
                    onChange={tabId => setTabId(tabId)}
                    selectedTabId={tabId}
                >
                    <Tab
                        disabled={isInProgress}
                        id={SIGN_IN}
                        title={SIGN_IN}
                        panel={
                            <SignIn
                                isButtonLoading={isInProgress}
                                onSignIn={onAttemptSignIn}
                            />
                        }
                    ></Tab>
                    <Tab
                        disabled={isInProgress}
                        id={SIGN_UP}
                        title={SIGN_UP}
                        panel={
                            <SignUp
                                isButtonLoading={isInProgress}
                                onSignUp={onAttemptSignUp}
                            />
                        }
                    ></Tab>
                </Tabs>
            </Dialog>
            <Toaster>
                {showError && (
                    <Toast
                        intent={Intent.DANGER}
                        message="Please fill required fields"
                        onDismiss={() => setShowError(false)}
                    ></Toast>
                )}
            </Toaster>
        </>
    );
};

LoginPage.propTypes = {
    onClose: propTypes.func.isRequired,
    isOpen: propTypes.bool,
    inProgress: propTypes.bool,
    onSignIn: propTypes.func.isRequired,
    onSignUp: propTypes.func.isRequired
};

LoginPage.defaultTypes = {
    isOpen: false,
    inProgress: false
};

export default LoginPage;
