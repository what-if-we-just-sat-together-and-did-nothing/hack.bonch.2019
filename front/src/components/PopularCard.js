import React from "react";
import { Icon } from "@blueprintjs/core";
import "../styles/PopularCard.scss";
import axios from "axios";

class PopularCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visitorsCount: this.props.visitors,
      showSubscribePopup: false,
      subscribed: false,
      email: "",
      rightEmail: false
    };

    this.subscribeOnEvent = this.subscribeOnEvent.bind(this);
    this.openSubscribePopup = this.openSubscribePopup.bind(this);
    this.closeSubscribePopup = this.closeSubscribePopup.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
  }

  openSubscribePopup() {
    this.setState(() => ({
      showSubscribePopup: true
    }));
  }

  closeSubscribePopup() {
    this.setState(() => ({
      showSubscribePopup: false
    }));
  }

  handleEmailChange(event) {
    event.persist();
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let test = re.test(String(event.target.value).toLowerCase());
    this.setState(() => ({
      email: event.target.value,
      rightEmail: test
    }));
  }

  subscribeOnEvent() {
    this.setState(state => ({
      visitorsCount: state.visitorsCount + 1,
      showSubscribePopup: false,
      subscribed: true
    }));
    axios.post(
      `https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/events/${this.props.id}/subscribtions`,
      { email: this.state.email }
    );
  }

  render() {
    return (
      <div className="popular-card">
        <div className="header">
          <div className="name">
            <h1>{this.props.name}</h1>
            <p>
              {this.props.status === "waiting"
                ? this.props.date
                : "Планируется"}
            </p>
          </div>

          <div className="subscribe-count">
            <Icon icon="people" />
            <p>{this.props.visitors}</p>
          </div>
        </div>
        <div className="content">
          <p>{this.props.desc}</p>
        </div>
        <div className="utilities">
          {!this.state.subscribed ? (
            <div className="subscribe-button" onClick={this.openSubscribePopup}>
              <Icon icon="walk" class="icon" />
              {this.props.status === "waiting" ? "Я иду!" : "Я бы сходил!"}
            </div>
          ) : (
            <div className="subscribe-button subscribed">
              <Icon icon="tick" />
              Вы подписались!
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default PopularCard;
