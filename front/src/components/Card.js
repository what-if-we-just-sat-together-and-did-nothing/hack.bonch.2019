import React from 'react';

import {Icon, Dialog, Classes} from "@blueprintjs/core";
import axios from "axios";
import { Button, Card as BlueprintCard, Elevation } from "@blueprintjs/core";

import '../styles/Card.scss';


class Card extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            visitorsCount: this.props.visitors,
            showSubscribePopup: false,
            subscribed: false,
            email: '',
            rightEmail: false
        };

        this.subscribeOnEvent = this.subscribeOnEvent.bind(this);
        this.openSubscribePopup = this.openSubscribePopup.bind(this);
        this.closeSubscribePopup = this.closeSubscribePopup.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
    }

    openSubscribePopup() {
        this.setState(() => ({
            showSubscribePopup: true
        }))
    }

    closeSubscribePopup() {
        this.setState(() => ({
            showSubscribePopup: false
        }))
    }

    handleEmailChange(event) {
        event.persist();
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let test = re.test(String(event.target.value).toLowerCase());
        this.setState(() => ({
            email: event.target.value,
            rightEmail: test,
        }))
    }

    subscribeOnEvent() {
        this.setState(state => ({
            visitorsCount: state.visitorsCount + 1,
            showSubscribePopup: false,
            subscribed: true
        }));
        axios.post(`https://erznuglpoe479921ka26glc2s836xnzw.cf:8802/api/events/${this.props.id}/subscribtions`, {email: this.state.email})
    }

    render() {
        return (
            <BlueprintCard className="card" interactive={false} elevation='1'>
                <div className="content">
                    <div className="header">
                        <div className="name">
                            <h1>{this.props.name}</h1>
                            <div className="address">
                                <p>{this.props.status === 'waiting' ? `${this.props.subway}, ${this.props.address}` : 'Планируется'}</p>
                                {this.props.status === 'waiting' ? <Icon icon="map-marker"/> : ''}
                            </div>
                        </div>
                        <div className="date">
                            <p>{this.props.status === 'waiting' ? this.props.date : ''}</p>
                        </div>
                    </div>
                    <p>{this.props.desc}</p>
                    <div className="utilities">
                        {!this.state.subscribed ? (
                            <div className="subscribe-button" onClick={this.openSubscribePopup}>
                                <Icon icon="walk"/>{this.props.status === 'waiting' ? 'Я иду!' : 'Я бы сходил!'}
                            </div>
                        ) : (
                            <div className="subscribe-button subscribed">
                                <Icon icon="tick"/>Вы подписались!
                            </div>
                        )}
                        <div className="subscribe-count">
                            <Icon icon="people"/>
                            <p>{this.state.visitorsCount}</p>
                        </div>
                    </div>
                </div>
                <div className="image">
                    <img src={this.props.img}></img>
                </div>
                <Dialog isOpen={this.state.showSubscribePopup}
                        onClose={this.closeSubscribePopup}
                        title=''
                        usePortal={true}
                >
                    <div className={Classes.DIALOG_BODY}>
                        <p>На этот адрес электронной почты мы вышлем вам письмо с подтверждением регистрации на
                            ивент!</p>
                        <div className="bp3-input-group">
                            <Icon icon="envelope"/>
                            <input value={this.state.email} onChange={this.handleEmailChange} type="text"
                                   className="bp3-input" placeholder="Введите электронную почту"/>
                        </div>
                        {this.state.rightEmail ? (
                            <div className="dialog-subscribe-button" onClick={this.subscribeOnEvent}>
                                Подписаться
                            </div>
                        ) : (
                            <div className="dialog-subscribe-button disabled" onClick={this.subscribeOnEvent}>
                                Подписаться
                            </div>
                        )}
                    </div>
                </Dialog>
            </BlueprintCard>
        );
    }

}

export default Card;
