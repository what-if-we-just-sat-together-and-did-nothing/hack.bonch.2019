import React from 'react';
import PopularCard from '../components/PopularCard';
import {fetchPopularEvents} from '../api/events';
import moment from "moment";

class PopularTable extends React.Component {

    componentDidMount() {
        fetchPopularEvents();
    }

    render() {
        const {popularEvents} = this.props;
        const dateFormatter = (event) => moment(event.start_time).format('DD-MM-YYYY, HH:MM');
        return (
            <div className="popular-trade">
                {popularEvents.map((event) =>
                    <PopularCard key={event.id} name={event.title} address={event.address} desc={event.description}
                                 date={dateFormatter(event)} visitors={event.visitors} status={event.status}/>
                )}
            </div>
        )
    }
}

export default PopularTable;
