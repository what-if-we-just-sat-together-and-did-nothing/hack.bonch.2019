import React from "react";
import { YMaps, ZoomControl, Map, Placemark } from "react-yandex-maps";
import '../styles/MapPage.scss';

function MapPage() {
  return (
    <div className="map-page">
      <div className="map-container">
        <h1>События на карте</h1>
        <YMaps onLoad={(ymaps) => console.log(ymaps)}>
          <Map
            defaultState={{
              center: [59.938, 30.3],
              zoom: 9,
              controls: []
            }}
            defaultOptions={{
              restrictMapArea: [
                [59.838, 29.511],
                [60.056, 30.829]
              ]
            }}
            width="63vw"
            height="80vh"
          >
            <ZoomControl options={{ float: "right" }} />
            <Placemark geometry={[59.902847, 30.488936]} />
            <Placemark geometry={[59.927447, 30.353834]} />
            <Placemark geometry={[59.919081, 30.344135]} />
          </Map>
        </YMaps>
      </div>
      <div className="table-popular-container">
        <h1>Популярное</h1>
      </div>
    </div>
  );
}

export default MapPage;
