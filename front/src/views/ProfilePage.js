import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Profile from "../components/Profile";
import {createEvent} from '../api/events'
import EventTable from "../components/EventTable";
import EventForm from "../components/EventForm";
import { Icon, Dialog } from "@blueprintjs/core";
import {getProfile} from '../api/user';
import "../styles/ProfilePage.scss";

function ProfilePage({user, events}) {
        useEffect(() => {
            if (!user.email && user.id) {
                getProfile(user);
            }
        }, []);
    const [isAddEventDisplaying, setIsAddEventDisplaying] = useState(false);
    return (
        <>
            <div className="profile-page-container">
                <div className="profile-page-event-table">
                    <EventTable events={events}></EventTable>
                    <div className="add-event-button" onClick={setIsAddEventDisplaying.bind(null, true)}>
                        <Icon icon="add"></Icon>
                        Создать новый ивент
                    </div>
                </div>
                <Profile user={user} className="profile"></Profile>
            </div>
            <Dialog
                className="event-dialog"
                isOpen={isAddEventDisplaying}
                onClose={setIsAddEventDisplaying.bind(null, false)}
                title='Add event'
            >
                <EventForm onCreate={createEvent}></EventForm>
            </Dialog>
        </>
    );
}

const mapStateToProps = state => {
    return {
        user: state.user,
        events: state.events.events
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
