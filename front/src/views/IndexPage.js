import React from 'react';
import {connect} from 'react-redux';
import '../styles/IndexPage.scss';
import EventTable from '../components/EventTable';
import PopularTable from "../components/PopularTable";

function IndexPage({events, popularEvents}) {
    return (
        <div className="index-page">
            <div className="table-events-container">
                <h1>Куда сходить</h1>
                <EventTable events={events} ></EventTable>
            </div>
            <div className="table-popular-container">
                <h1>Популярное</h1>
                <PopularTable popularEvents={popularEvents}></PopularTable>
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        events: state.events.events,
        popularEvents: state.events.popularEvents,   
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
